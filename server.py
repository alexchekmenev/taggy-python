#!/usr/bin/env python
import json
from http.server import BaseHTTPRequestHandler, HTTPServer
from operator import itemgetter
from urllib.parse import urlparse, parse_qs

import numpy as np
from keras.applications import vgg16, inception_v3, resnet50, mobilenet, mobilenet_v2
from keras.applications.imagenet_utils import decode_predictions
from keras.preprocessing.image import img_to_array
from keras.preprocessing.image import load_img, save_img


class Item:
    def __init__(self, tag, score):
        self.tag = tag
        self.score = score

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, sort_keys=True, indent=4)


# HTTPRequestHandler class
class RequestHandler(BaseHTTPRequestHandler):

    # GET
    def do_GET(self):
        query_components = parse_qs(urlparse(self.path).query)

        if "filename" in query_components and len(query_components["filename"]) > 0:
            filename = "../images/" + query_components["filename"][0]
            original = load_img(filename, target_size=(224, 224))
            numpy_image = img_to_array(original)
            image_batch = np.expand_dims(numpy_image, axis=0)

            # plt.imshow(np.uint8(image_batch[0]))
            # save_img("output_" + filename, original)

            processed_image = vgg16.preprocess_input(image_batch.copy())
            processed_image1 = inception_v3.preprocess_input(image_batch.copy())
            processed_image2 = resnet50.preprocess_input(image_batch.copy())
            processed_image3 = mobilenet.preprocess_input(image_batch.copy())
            processed_image4 = mobilenet_v2.preprocess_input(image_batch.copy())

            predictions = vgg_model.predict(processed_image)
            predictions1 = inception_model.predict(processed_image1)
            predictions2 = resnet_model.predict(processed_image2)
            predictions3 = mobilenet_model.predict(processed_image3)
            predictions4 = mobilenet_v2_model.predict(processed_image4)

            # Send message back to client
            labels = [
                decode_predictions(predictions),
                decode_predictions(predictions1),
                decode_predictions(predictions2),
                decode_predictions(predictions3),
                decode_predictions(predictions4)
            ]
            # print(labels)

            results = []
            for label in labels:
                for item in label[0]:
                    _, tag, score = item
                    score = score.item()
                    # print(tag, score)
                    tmp = Item(tag, score).__dict__
                    results.append(tmp)
            results = sorted(results, key=itemgetter('score'), reverse=True)
            json_string = "{\"result\": " + json.dumps(results) + "}"
        else:
            json_string = "{\"error\": \"filename is missing\"}"

        # Send response status code
        self.send_response(200)

        # Send headers
        self.send_header('Content-type', 'application/json')
        self.end_headers()

        # Write content as utf-8 data
        self.wfile.write(bytes(json_string, "utf8"))
        return


def main():
    print('starting server...')

    # Server settings
    # Choose port 8080, for port 80, which is normally used for a http server, you need root access
    server_address = ('0.0.0.0', 8081)
    httpd = HTTPServer(server_address, RequestHandler)
    print('running server...')
    httpd.serve_forever()


# Load the VGG model
vgg_model =  vgg16.VGG16(weights='imagenet')

# Load the Inception_V3 model
inception_model = inception_v3.InceptionV3(weights='imagenet')

# Load the ResNet50 model
resnet_model = resnet50.ResNet50(weights='imagenet')

# Load the MobileNet model
mobilenet_model = mobilenet.MobileNet(weights='imagenet')

mobilenet_v2_model = mobilenet_v2.MobileNetV2(weights='imagenet')

main()
